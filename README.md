# Onmixmonitoring

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
Chose one of these commands for build the project in relation with the environment:
- `npm run build:dev` for a developed build.
- `npm run build:pre` for a pre-production build.
- `npm run build:prod` for a production build.

## Deploy
First, you should install [firebase-cli](https://firebase.google.com/docs/cli)
In Linux, run  `curl -sL https://firebase.tools | bash`.

After it is installed, run deploy sentences for a specific environment:
 - `npm run deploy:dev` for dev environment.
 - `npm run deploy:pre` for pre environment.
 - `npm run deploy:prod` for prod environment.

During this deployment process, firebase email account and password will be requestes.
For dev and pre environments are in [confluence](https://mediatech.atlassian.net/wiki/spaces/RETAIL/pages/338788423/Realtime+monitoring+system+for+client+terminals).

Note: Not yet try to deploy in production, pending credentials from support team and 
check name of firebase project for change in package.json.
For check current list of firebase projects, run `firebase projects:list`.
