﻿import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AppRoutes } from './../app-routes';
import { AuthService } from '../login/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router,
              private authFirebaseService: AuthService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const userAuthenticated = this.authFirebaseService.isLoggedIn;
    if (userAuthenticated) {
      return true;
    }
    this.router.navigate([AppRoutes.login]);
    return false;
  }
}
