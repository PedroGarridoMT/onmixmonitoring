import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigService } from './config.service';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';


export function InitApplicationFactory(config: ConfigService) {
  return () => config.load();
}

@NgModule({
  declarations: [],
  imports: [
    TranslateModule,
    CommonModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: InitApplicationFactory,
      deps: [ConfigService],
      multi: true,
    }
  ]
})
export class CoreModule {
}
