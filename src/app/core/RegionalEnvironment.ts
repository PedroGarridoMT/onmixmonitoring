export interface RegionalEnvironment {
    /**
     * Minimun password lenght.
     */
    minPassWordLength?: number;

    /**
     * The default player integration data account currency (The ISO 4217 numeric code of the currency).
     */
    currencyCode?: string;

    /**
     * Current country Code (Alpha-2 country code defined in ISO-3166-1).
     */
    countryCode?: string;

    /**
     * Need to show or not the second name.
     */
    showSecondName?: boolean;

    /**
     * Need to show or not the fiscal data.
     */
    showFiscalData?: boolean;

    /**
     * Should the Player's Security Answer be showed in the BackOffice's Player card or not
     */
    showSecurityQuestionAnswer?: boolean;

    /**
     * Need to show or not the additional document data.
     */
    showAdditionalDocumentData?: boolean;

    /**
     * Indicating whether address number is needed in the player registration
     */
    showAddressNumber?: boolean;

    /**
     * Country Code in whitch wil be validate the zip code (Alpha-2 country code defined in ISO-3166-1).
     */
    zipCodeValidator?: string;

}
