import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PlatformLocation } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { Language } from 'bowl';
import * as moment from 'moment';
import { RegionalEnvironment } from './RegionalEnvironment';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  private _appConfig: any;
  private _propertiesFileUrl: string;
  private _keyForStoredLanguage = 'user-stored-lang';
  private _defaultLanguage = 'en';
  private _environmentVariables: RegionalEnvironment;

  public URLs = {
    // _configService will be set in ConfigService constructor. It is used as a pointer to ConfigService instance.
    _configService: undefined,
    get environment() {
      return this._configService.getEndpoint('baseUrl') + '/config/regional-environment';
    }
  };

  constructor(private http: HttpClient,
              platformLocation: PlatformLocation,
              private translate: TranslateService) {
    this._propertiesFileUrl = `${ platformLocation.getBaseHrefFromDOM() }assets/properties.json`;
    this.URLs._configService = this;
  }

  load(): Promise<any> {
    return new Promise((resolve, reject) => {

      const environment = {
        minPassWordLength: 8,
        currencyCode: 'EUR',
        countryCode: 'es',
        showSecondName: false,
        showFiscalData: true,
        showSecurityQuestionAnswer: true,
        showAdditionalDocumentData: false,
        showAddressNumber: true,
        zipCodeValidator: 'ES'
      };

      this.http.get(this._propertiesFileUrl).subscribe(res => {
        this._appConfig = res;
        this._environmentVariables = environment;
        if (this._appConfig.keys['overrideEnv']) {
          this._environmentVariables = Object.assign(this._environmentVariables, this._appConfig.keys['overrideEnv']);
        }
        const baseLang = this.getLanguageToUse();
        moment.locale(baseLang);

        this.translate.use(baseLang)
          .subscribe(
            results => {
              this.translate.setDefaultLang(baseLang);
              resolve(results);
            },
            translationError => {
              console.error('Error loading translation files!!!', translationError);
              reject(translationError);
            });
      });
    });
  }

  getEndpoint(endpoint: string): string {
    return this._appConfig.endpoints[endpoint];
  }

  getKey(key: string): any {
    return this._appConfig.keys[key];
  }


  getEnvironmentVariables(): RegionalEnvironment {
    return this._environmentVariables;
  }

  setLanguageAndLocale(lang: string): void {
    moment.locale(lang);
    localStorage.setItem(this._keyForStoredLanguage, lang);
    this.translate.setDefaultLang(lang);
    this.translate.use(lang);
    // this.translate.use(lang + '-' + this._environmentVariables['countryCode']);
  }

  private languageIsAvailable(lang: string): boolean {
    const applicationLangs = this.getKey('languages') as Language[];
    return lang && applicationLangs.some(item => item.value === lang);
  }

  private getUserLanguagePreference(): string {
    const userLang = localStorage.getItem(this._keyForStoredLanguage);
    return userLang ? userLang : null;
  }

  private getLanguageToUse(): string {
    const storedLang = this.getUserLanguagePreference();
    const browserLang = this.translate.getBrowserLang();

    if (this.languageIsAvailable(storedLang)) {
      return storedLang;
    } else if (this.languageIsAvailable(browserLang)) {
      return browserLang;
    } else {
      return this._defaultLanguage;
    }
  }

}
