import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  dbService: any;
  items: any;
  destroy$ = new Subject<any>();

  constructor(db: AngularFireDatabase) {
    this.dbService = db;
  }

  ngOnInit(): void {

    this.dbService.list('STATUS/').snapshotChanges()
      .pipe(takeUntil(this.destroy$))
      .subscribe((response) => {
        this.items = response;
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }
}
