export class AppRoutes {
  public static home = '/';
  public static login = '/login';
  public static pageNotFound = '/page-not-found';
  public static accessForbidden = '/forbidden';
  public static settings = '/settings';
  public static monitoring = '/monitoring';
}
