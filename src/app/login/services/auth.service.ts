import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import { from, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: User;
  isLogged: boolean;

  constructor(
    public afAuth: AngularFireAuth,
    public router: Router
  ) {
    this.subscribeToFirebaseAuthState();
  }

  private subscribeToFirebaseAuthState(): void {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.user = user;
        localStorage.setItem('user', JSON.stringify(this.user));
      } else {
        localStorage.setItem('user', null);
      }
    });
  }

  login(email: string, password: string): Observable<any> {
    return from(this.afAuth.auth.signInWithEmailAndPassword(email, password));
  }

  logout(): Observable<any> {
    this.isLogged = false;
    return from(this.afAuth.auth.signOut())
      .pipe(tap(() => localStorage.removeItem('user')));
  }

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user !== null || this.isLogged;
  }

  set isLoggedIn(logged: boolean) {
    this.isLogged = logged;
  }
}
