import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { LoginLiterals, LoginModel } from 'bowl';
import { Subject, Subscription } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AppRoutes } from '../../app-routes';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  destroy$ = new Subject<any>();

  currentError = '';
  loading = false;
  logoIcon = './assets/img/onmix-full-logo.png';
  loginLiterals: LoginLiterals = {} as LoginLiterals;

  constructor(
    private authService: AuthService,
    private router: Router,
    private translate: TranslateService,
  ) {
  }

  ngOnInit(): void {
    this.loginLiterals.login = this.translate.instant('login.button');
    this.loginLiterals.password = this.translate.instant('login.password');
    this.loginLiterals.username = this.translate.instant('login.username');
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  onSubmit(credentials: LoginModel): Subscription {
    this.currentError = '';
    this.loading = true;
    return this.authService.login(credentials.username, credentials.password)
      .pipe(
        takeUntil(this.destroy$),
        finalize(() => {this.loading = false})
      )
      .subscribe(
        success => this.loginSuccess(success),
        error => this.loginError(error)
      );
  }

  private loginSuccess(success: any): void {
    this.authService.isLoggedIn = true;
    this.router.navigate([AppRoutes.monitoring]);
  }

  private loginError(error: any): void {
    this.currentError = this.mapError(error.code);
  }

  private mapError(code: string): string {
    switch (code) {
      case 'auth/wrong-password':
        return 'La contraseña es incorrecta';  // TODO CHANGE FOR i18n literals
      case 'auth/invalid-email':
        return 'El correo electrónico no tiene un formato válido'; // TODO CHANGE FOR i18n literals
      case 'auth/user-not-found':
        return 'No existe ningún usuario con este correo electrónico'; // TODO CHANGE FOR i18n literals
      default:
        return 'Existe algún problema, intentelo más tarde';
    }
  }
}
