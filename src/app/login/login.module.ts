import { NgModule } from '@angular/core';
import { LoginComponent } from './components/login.component';
import { AppRoutingModule } from '../app-routing.module';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MtAccountModule } from 'bowl';

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    AppRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    MtAccountModule
  ],
  exports: [
    LoginComponent
  ]
})
export class LoginModule {
}
