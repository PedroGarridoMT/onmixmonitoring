import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

export enum EmitterServiceEventsEnum { MENU_CURRENT_STATE_REQUEST, MENU_IS_COLLAPSED, MENU_IS_EXPANDED }

@Injectable({
  providedIn: 'root'
})
export class EmitterService {

  // Observable sources
  private eventBus = new Subject<string | EmitterServiceEventsEnum>();

  // Observable streams
  eventBus$ = this.eventBus.asObservable();

  emitEvent(eventName: string | EmitterServiceEventsEnum): void {
    // And notify subscribers
    this.eventBus.next(eventName);
  }
}
