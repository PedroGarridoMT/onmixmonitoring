import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { TitleBarParams } from '../model/title-bar.model';

export enum ItemNavigatorEventsEnum { NEXT_PAGE, PREVIOUS_PAGE }

@Injectable({
  providedIn: 'root'
})
export class TitleBarService {
  // Observable string sources
  private componentDestroyedSource = new Subject<void>();
  private componentInitializedSource = new Subject<TitleBarParams>();
  private actionRequestedSource = new Subject<string>();
  private itemNavigatorPageSource = new Subject<ItemNavigatorEventsEnum>();

  // Observable string streams
  componentDestroyed$ = this.componentDestroyedSource.asObservable();
  componentInitialized$ = this.componentInitializedSource.asObservable();
  actionRequested$ = this.actionRequestedSource.asObservable();
  itemNavigatorPaged$ = this.itemNavigatorPageSource.asObservable();

  // Service message commands
  componentInitialized(params: TitleBarParams): void {
    this.componentInitializedSource.next(params);
  }

  componentDestroyed(): void {
    this.componentDestroyedSource.next();
  }

  actionRequested(actionId: string): void {
    this.actionRequestedSource.next(actionId);
  }

  itemNavigatorPaged(itemNavigatorEventsEnum: ItemNavigatorEventsEnum): void {
    this.itemNavigatorPageSource.next(itemNavigatorEventsEnum);
  }
}
