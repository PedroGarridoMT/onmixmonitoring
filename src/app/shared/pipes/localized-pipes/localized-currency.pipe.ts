import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { LocalizedTransformPipe } from './localized-pipe';

@Pipe({ name: 'localizedcurrency', pure: false })
export class LocalizedCurrencyPipe
  extends LocalizedTransformPipe<CurrencyPipe>
  implements PipeTransform {

  constructor(translateService: TranslateService) {
    super(translateService, CurrencyPipe);
  }
}


