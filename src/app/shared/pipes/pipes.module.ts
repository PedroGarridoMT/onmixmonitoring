import { NgModule } from '@angular/core';
import { LocalizedCurrencyPipe } from './localized-pipes/localized-currency.pipe';
import { LocalizedDatePipe } from './localized-pipes/localized-date.pipe';
import { LocalizedDecimalPipe } from './localized-pipes/localized-decimal.pipe';

@NgModule({
  declarations: [
    LocalizedCurrencyPipe,
    LocalizedDatePipe,
    LocalizedDecimalPipe
  ],
  exports: [
    LocalizedCurrencyPipe,
    LocalizedDatePipe,
    LocalizedDecimalPipe
  ]
})
export class LocalizedPipesModule {
}
