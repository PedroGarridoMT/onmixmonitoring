import { Component, OnDestroy } from '@angular/core';
import { TitleBarService } from '../services/title-bar.service';
import { TitleBarParams } from '../model/title-bar.model';


@Component({
  template: ''
})
export class TitleBarHandlerComponent implements OnDestroy {
  titleBarService: TitleBarService;

  constructor(titleBarService: TitleBarService) {
    this.titleBarService = titleBarService;
  }

  notifyComponentInitialized(params: TitleBarParams) {
    this.titleBarService.componentInitialized(params);
  }

  ngOnDestroy(): void {
    this.titleBarService.componentDestroyed();
  }

}
