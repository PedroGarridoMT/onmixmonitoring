import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { BowlModule } from 'bowl';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TitleBarHandlerComponent } from './components/title-bar-handler.component';
import { LocalizedPipesModule } from './pipes/pipes.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


/**
 * AoT requires an exported function for factories
 */
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    TitleBarHandlerComponent
  ],
  imports: [
    CommonModule,
    BowlModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    LocalizedPipesModule,
    NgbModule
  ],
  exports: [
    TranslateModule,
    BowlModule,
    TitleBarHandlerComponent,
    LocalizedPipesModule
  ]
})
export class SharedModule {
}
