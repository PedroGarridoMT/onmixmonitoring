import { CustomNavigation, ItemNavigatorLiterals } from 'bowl';

export interface TitleBarActions {
  id: string;
  label: string;
  fontIcon?: string;
}

export interface ItemNavigatorParams {
  links?: Array<string>;
  literals?: ItemNavigatorLiterals;
  currentIndex: number;
  customNavigation?: CustomNavigation;
}

export class TitleBarParams {
  previousPagePath?: string;
  previousPageQueryParams?: {[key: string]: any};
  previousPageLabel?: string;
  pageTitle: string;
  pageActions?: TitleBarActions[];
  itemNavigator?: ItemNavigatorParams;
}
