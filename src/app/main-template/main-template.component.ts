import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { LoaderService, MenuItem, SlideMenuComponent } from 'bowl';
import { MainMenuService } from './services/main-menu.service';
import { EmitterService, EmitterServiceEventsEnum } from '../shared/services/emmiter.service';
import { ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, Router } from '@angular/router';
import { AppRoutes } from '../app-routes';
import { TitleBarParams } from '../shared/model/title-bar.model';
import { TitleBarService } from '../shared/services/title-bar.service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { AuthService } from '../login/services/auth.service';

@Component({
  selector: 'app-main-template',
  templateUrl: './main-template.component.html',
  styleUrls: ['./main-template.component.scss']
})
export class MainTemplateComponent implements OnInit, OnDestroy {

  @ViewChild(SlideMenuComponent, { static: true }) mobileMenu: SlideMenuComponent;
  menuItems: MenuItem[];
  userOptions: any[];
  userName: string;
  titleBarContent: TitleBarParams;
  homeRoute: string = null;

  activeState: string;
  menuCollapsed: boolean;
  menuLogoIcon = './assets/img/onmix-logo.png';
  menuLogoLink = AppRoutes.monitoring;
  menuLogoText = './assets/img/onmix-text.png';
  menuLogoTextClass = 'onmix-logo-size';


  private subscriptions: Subscription[] = [];


  constructor(
    private mainMenuService: MainMenuService,
    private cd: ChangeDetectorRef,
    private emitterService: EmitterService,
    private loader: LoaderService,
    private titleBarService: TitleBarService,
    private translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private authService: AuthService,
  ) {
    this.getConfigDataFromRoutes();
  }


  ngOnInit(): void {
    this.initializeComponents();

    // Subscribe to language change events. If any happens => re-initialize components in the new language
    this.translate.onLangChange.subscribe(() => {
      this.initializeComponents();
    });

    this.subscriptions.push(
      // Subscribe to default language change events. If any happens => re-initialize components in the new language
      this.translate.onDefaultLangChange.subscribe(() => {
        this.initializeComponents();
      }),
      // Subscribe to component initialized events so the title bar paints the ui elems according to parameters received.
      this.titleBarService.componentInitialized$.subscribe(data => {
        this.titleBarContent = data;
        this.setItemNavigatorLiterals();
        this.cd.detectChanges();
      }),
      // Subscribe to events launched when the components are destroyed so the title bar can be cleaned
      this.titleBarService.componentDestroyed$.subscribe(() => {
        this.titleBarContent = null;
      }),
      //
      this.emitterService.eventBus$.subscribe(event => {
        if (event === EmitterServiceEventsEnum.MENU_CURRENT_STATE_REQUEST) {
          this.emitMenuStateEvent(this.menuCollapsed);
        }
      })
    );
    this.router.navigate([AppRoutes.monitoring]);
  }


  ngOnDestroy(): void {
  }


  /**
   * Create the menu items to be passed to the mt-menu components.
   */
  private initializeComponents(): void {

    this.menuItems = this.mainMenuService.getLateralMenu();
    this.userOptions = this.mainMenuService.createUserDropdownOptions();
    this.userName = 'test'; // todo get username
  }

  onToggleMenu(collapsed: boolean) {
    this.menuCollapsed = collapsed;
    this.cd.detectChanges();
    this.emitMenuStateEvent(collapsed);
  }

  onToggleMobileMenu() {
    this.mobileMenu.toggleMenu();
  }

  emitMenuStateEvent(isMenuCollapsed: boolean) {
    isMenuCollapsed ?
      this.emitterService.emitEvent(EmitterServiceEventsEnum.MENU_IS_COLLAPSED) :
      this.emitterService.emitEvent(EmitterServiceEventsEnum.MENU_IS_EXPANDED);
  }


  onUserMenuClicked(param: string) {
    switch (param.toLowerCase()) {
      case 'logout':
        this.loader.show();
        this.authService.logout().subscribe(() => {
          this.loader.hide();
          this.router.navigate([AppRoutes.login]);
        });
        break;

      // case 'change-password':
      //   this.showChangePasswordModal();
      //   break;

      case 'settings':
        this.router.navigate([
          AppRoutes.settings
        ]);
        break;
    }
  }


  /**
   *  Title bar functions
   */
  onTitleBarActionClick(actionId: string) {
    // Notify the title bar services so the owner of this action can handle it.
    this.titleBarService.actionRequested(actionId);
  }


  getPreviousPagePath() {
    if (!this.titleBarContent || !this.titleBarContent.previousPagePath) {
      return '#';
    }
    const path = this.titleBarContent.previousPagePath;
    return path.startsWith('/') ? [path] : ['/' + path];
  }


  getPreviousPageQueryParams() {
    if (!this.titleBarContent || !this.titleBarContent.previousPageQueryParams) {
      return {};
    }
    return this.titleBarContent.previousPageQueryParams;
  }


  /**
   * If no literals are specified for the item-navigator component, get default ones.
   * mt-item-navigator literals
   */
  private setItemNavigatorLiterals() {
    if (this.titleBarContent.itemNavigator && !this.titleBarContent.itemNavigator.literals) {
      this.titleBarContent.itemNavigator.literals = {
        previous: this.translate.instant('common.navigator.previous'),
        next: this.translate.instant('common.navigator.next'),
        label: this.translate.instant('common.navigator.label'),
      };
    }
  }


  /**
   * Everytime a NavigationEnd event occurs, this method get the data stored on the current route. (See app-routing.module.ts)
   * This data contains information about the currentStateName, which is used to mark the active section in the menu.
   * And title information, used to update the page title according to the current route.
   */
  getConfigDataFromRoutes() {
    this.subscriptions.push(
      this.router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {

          // Store all params from the root route => To be used in the page title
          const urlParams: any = {};
          this.route.children.forEach(route => route.params.subscribe(params => {
            Object.keys(params).forEach(key => urlParams[key] = params[key]);
          }));

          const currentRouteSnapshot = this.getRouteSnapshot();

          // Store active state used in the menus
          this.activeState = currentRouteSnapshot.data.stateName || null;

          // Set page title
          this.setPageTitle(currentRouteSnapshot, urlParams);

          // Scroll top every time there is a route change.
          window.scrollTo(0, 0);
        }
      })
    );
  }


  getRouteSnapshot(): ActivatedRouteSnapshot {
    // Traverse the active route tree to access the latest route
    let snapshot = this.route.snapshot;
    let activated = this.route.firstChild;
    if (activated != null) {
      while (activated != null) {
        snapshot = activated.snapshot;
        activated = activated.firstChild;
      }
    }
    return snapshot;
  }


  setPageTitle(routeSnapshot, urlParams) {
    const pageTitle = 'OnMix';

    // Find title config from the data object stored in the route
    const titleConfig = routeSnapshot.data.title || null;
    if (titleConfig) {
      this.translate.get(titleConfig.text).subscribe(title => {
        if (titleConfig.param) {
          title = title.replace('{{' + titleConfig.param + '}}', urlParams[titleConfig.param]);
        }
        this.titleService.setTitle(title);
      });
    } else {
      this.titleService.setTitle(pageTitle);
    }
  }

}
