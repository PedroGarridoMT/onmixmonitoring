import { Injectable } from '@angular/core';
import { MenuItem } from 'bowl';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class MainMenuService {
  constructor(
    private translate: TranslateService,
  ) {
  }

  getLateralMenu(): MenuItem[] {
    return [
      this.getMonitoring()
    ];
  }

  private getMonitoring(): MenuItem {
    return {
      stateName: 'Monitoring',
      text: this.translate.instant('menu.monitoring'),
      targetRoute: '/monitoring',
      iconClasses: 'fas fa-chart-bar',
      cssSelector: 'menu-monitoring',
      children: []
    };
  }

  createUserDropdownOptions(): any {
    return [
      { param: 'settings', text: this.translate.instant('account.settings'), cssSelector: 'settings-selector' },
      { param: 'logout', text: this.translate.instant('account.logout'), cssSelector: 'logout-selector' }
    ];
  }

}
