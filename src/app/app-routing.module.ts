import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/components/login.component';
import { MonitoringComponent } from './monitoring/components/monitoring.component';
import { MainTemplateComponent } from './main-template/main-template.component';
import { AuthGuard } from './core/can-activate-guard.service';
import { SettingsMainComponent } from './settings/components/settings-main/settings-main.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: '', component: MainTemplateComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'monitoring', component: MonitoringComponent,
        data: {
          stateName: 'Monitoring',
          title: { text: 'pageTitle.monitoring.main' }
        },
      },
      {
        path: 'settings', component: SettingsMainComponent,
        data: {
          stateName: 'Settings',
          title: { text: 'pageTitle.settings.main' }
        }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
