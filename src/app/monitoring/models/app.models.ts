import { TerminalStatusBase } from './db.models';

export enum FirebaseEventType {
  child_added = 'child_added',
  child_changed = 'child_changed',
  child_removed = 'child_removed',
  child_moved = 'child_moved'
}

export interface TerminalStatusApp extends TerminalStatusBase {
  TerminalAcknowledgement?: Date;
  TerminalStart?: Date;
}


export interface DocumentApp<T> {
  id: string;
  payload: T;
}

export interface DocumentAppEvent<T> extends DocumentApp<T> {
  type: FirebaseEventType;
}

export interface DocumentAppUpdate<T> extends DocumentApp<T> {
  lastUpdate: number;
}


export interface CenterActiveCount {
  total: number;
  actives: number;
}

export interface TerminalIdentifier {
  center: string;
  terminal: string;
}

export enum AliveStatus {
  OK,
  WARNING,
  KO
}

export enum ErrorStatus {
  OK = 0,
  WARNING,
  ERROR
}

export interface DateInputModel {
  year?: number;
  month?: number;
  day?: number;
  hour?: number;
  minute?: number;
  second?: number;
}
