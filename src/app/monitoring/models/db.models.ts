export enum FrontStatus {
    UP = 'UP',
    DOWN = 'DOWN'}

export enum Scenario {
    Dashboard = 'Dashboard' ,
    InGame ='InGame' ,
    InLotteryGame = 'InLotteryGame' ,
    Tester_Basic = 'Tester_Basic' ,
    Tester_Pro ='Tester_Pro'}

export enum Status {
    Nolock = 'Nolock' ,
    UnexpectedError = 'UnexpectedError' ,
    RemoteLock = 'RemoteLock' ,
    LocalLock = 'LocalLock' ,
    LostConnection = 'LostConnection' ,
    OpenMainDoor = 'OpenMainDoor' ,
    HoppersOut = 'HoppersOut' ,
    OpenDownDoor = 'OpenDownDoor' ,
    PrizeRecived = 'PrizeRecived' ,
    ManualPayment = 'ManualPayment' ,
    InitPayment = 'InitPayment' ,
    AskPayment = 'AskPayment' ,
    PaymentServiceError = 'PaymentServiceError'}


export enum GeneralStatus {
    OK = 'OK',
    WARNING = 'WARNING',
    ERROR = 'ERROR'}


export interface TerminalStatusBase {
    ClientSoftwareVersion?: string,
    EmptyHopperProcess?: boolean
    FrontStatus?: FrontStatus
    RefillInProccess?: boolean,
    Scenario?: Scenario,
    Status?: Status
    TerminalAcknowledgement?: any,
    TerminalGeneralStatus?: GeneralStatus
    TerminalStart?: any
}

export interface TerminalStatusDB extends TerminalStatusBase{
    TerminalAcknowledgement?: string,
    TerminalStart?: string
}

export interface CenterStatusDB {
    [terminal:string] : TerminalStatusDB
}

export interface StatusDB {
    [center:string] : CenterStatusDB
}
