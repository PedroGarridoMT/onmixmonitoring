import { Injectable } from '@angular/core';
import { AliveStatus, ErrorStatus } from '../models/app.models';
import { GeneralStatus, Status, TerminalStatusDB } from '../models/db.models';

const terminalAckIntervalMillis = 10000;

const config = {
  terminalAckIntervalMillis: terminalAckIntervalMillis,
  terminalAckWarningMillis: terminalAckIntervalMillis * 1.5,
  terminalAckErrorMillis: terminalAckIntervalMillis * 3
};

@Injectable({
  providedIn: 'root'
})
export class TerminalCheckerProvider {

  public get config(): any {
    return config;
  }

  public aliveStatus(terminal: TerminalStatusDB): AliveStatus {
    const ackDate = this.parseDate(terminal.TerminalAcknowledgement);
    if (!ackDate) {
      return AliveStatus.KO;
    }
    const tillLastAckMillis = (Date.now() - ackDate.getTime());
    if (tillLastAckMillis >= config.terminalAckErrorMillis) {
      return AliveStatus.KO;
    } else if (tillLastAckMillis >= config.terminalAckWarningMillis) {
      return AliveStatus.WARNING;
    } else {
      return AliveStatus.OK;
    }
  }

  public isLocked(terminal: TerminalStatusDB): boolean {
    return terminal.Status === Status.LocalLock ||
      terminal.Status === Status.RemoteLock;
  }

  public errorStatus(terminal: TerminalStatusDB): ErrorStatus {
    switch (terminal.TerminalGeneralStatus) {
      case GeneralStatus.OK:
        return ErrorStatus.OK;
      case GeneralStatus.WARNING:
        return ErrorStatus.WARNING;
      case GeneralStatus.ERROR:
        return ErrorStatus.ERROR;
    }
  }

  private parseDate(dateString: string): Date | null {
    const result = new Date(dateString);
    return isNaN(result.getTime()) ? null : result;
  }
}
