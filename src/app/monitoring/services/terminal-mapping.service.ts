import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TerminalMappingService {

  private static getGeneralStatus(backendStatus: string, frontendStatus: string): string {
    if (backendStatus === 'WARNING' || backendStatus === 'ERROR') {
      return backendStatus;
    }
    return frontendStatus === 'UP' ? 'OK' : 'WARNING_FRONTEND';
  }

  private static createTerminalObject(centerName: string, terminalId: any, payload: any): any {
    const concatTerminal = Object.assign({}, { CenterName: centerName, TerminalId: terminalId }, payload);
    concatTerminal.Scenario = 'monitoring.scenario.' + concatTerminal.Scenario.toLowerCase();

    concatTerminal.GeneralStatus = TerminalMappingService.getGeneralStatus(
      concatTerminal.TerminalGeneralStatus, concatTerminal.FrontStatus
    );
    concatTerminal.Status = concatTerminal.GeneralStatus === 'WARNING_FRONTEND' ?
      'monitoring.status.nofrontend' :
      'monitoring.status.' + concatTerminal.Status.toLowerCase();

    concatTerminal.ClientType = 'monitoring.clientType.' + concatTerminal.ClientType.toLowerCase();
    return concatTerminal;
  }


  public mappingTerminals(response, centerNames): any {
    const terminalMapping = [];
    const terminalsList = centerNames ? response.filter(terminal => this.filterCenterNames(centerNames, terminal)) : response;
    terminalsList.forEach((item) => {
      const payload = item.payload.val();
      const keys = Object.keys(payload);
      if (payload[keys[0]].ClientType) {
        const centerName = item.key;
        const terminalList = item.payload.val();
        const terminalsIdsList = Object.keys(terminalList);
        terminalsIdsList.forEach((terminalId) =>
          terminalMapping.push(TerminalMappingService.createTerminalObject(centerName, terminalId, terminalList[terminalId]))
        );
      }
      // Not show devices of new core
      // else {
        // keys.forEach(centerName => {
        //   if ((centerNames && centerNames.indexOf(centerName) > -1) || !centerNames) {
        //     const deviceUids = Object.keys(payload[centerName]);
        //     deviceUids.forEach(deviceUid => {
        //       terminalMapping.push(TerminalMappingService.createTerminalObject(centerName, deviceUid, payload[centerName][deviceUid]));
        //     });
        //   }
        // });
      // }
    });
    return terminalMapping;
  }


  private filterCenterNames(centerNames, item): boolean {
    if (centerNames.indexOf(item.key) > -1) {
      return true;
    }
    const payload = item.payload.val();
    const keys = Object.keys(payload);
    if (!payload[keys[0]].ClientType) {
      return true;
    }
    return false;
  }

  public getCenterNameList(response): Array<string> {
    const centerNameList: Array<string> = [];
    response.forEach((item) => {
      const payload = item.payload.val();
      const keys = Object.keys(payload);
      if (payload[keys[0]].ClientType) {
        centerNameList.push(item.key);
      }
      // Not show centers of new core
      // else {
        // centerNameList = centerNameList.concat(keys);
      // }
    });
    return centerNameList;
  }
}
