import { Injectable } from '@angular/core';
import { FilterBarItem, FilterBarLiterals, FilterType, TableColumn, TableColumnType, TableColumnVisibilityEnum, TableLiterals } from 'bowl';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class MonitoringFactory {

  constructor(private translate: TranslateService) {
  }

  getFiltersBarLiterals(): FilterBarLiterals {
    return {
      search: this.translate.instant('filtersBar.search'),
      clear: this.translate.instant('filtersBar.clear'),
      clearAll: this.translate.instant('filtersBar.clearAll'),
      selectAll: this.translate.instant('filtersBar.selectAll'),
      showAllFilters: this.translate.instant('filtersBar.showAllFilters'),
      showDefaultFilters: this.translate.instant('filtersBar.showDefaultFilters'),
      rangeFrom: this.translate.instant('filtersBar.rangeFrom'),
      rangeTo: this.translate.instant('filtersBar.rangeTo'),
      invalidDate: this.translate.instant('forms.error.invalidDate'),
      invalidRange: this.translate.instant('forms.error.invalidRange'),
      dayCode: this.translate.instant('common.date.dayCode'),
      yearCode: this.translate.instant('common.date.yearCode'),
      emptySearchMessage: this.translate.instant('filtersBar.emptySearchMessage'),
    };
  }

  getTableLiterals(): TableLiterals {
    return {
      noResultsMessage: this.translate.instant('table.noResultsMessage'),
      columnsVisibilityLabel: this.translate.instant('table.columnsVisibilityLabel'),
      exportButtonLabel: this.translate.instant('table.exportButtonLabel'),
      totalItemsLabel: this.translate.instant('table.totalItemsLabel'),
      pageLabel: this.translate.instant('table.pageLabel'),
      actionsLabel: this.translate.instant('table.actionsLabel'),
      itemsLabel: this.translate.instant('table.itemsLabel')
    };
  }

  getColumns(): TableColumn[] {
    return [
      {
        type: TableColumnType.DATA,
        id: 'CenterName',
        label: this.translate.instant('monitoring.entity.centerName'),
        visibility: TableColumnVisibilityEnum.VISIBLE,
        sortable: true
      },
      {
        type: TableColumnType.DATA,
        id: 'TerminalId',
        label: this.translate.instant('monitoring.entity.terminalId'),
        visibility: TableColumnVisibilityEnum.VISIBLE,
        sortable: false
      },
      {
        type: TableColumnType.DATA,
        id: 'ClientSoftwareVersion',
        label: this.translate.instant('monitoring.entity.clientVersion'),
        visibility: TableColumnVisibilityEnum.VISIBLE,
        sortable: false
      },
      {
        type: TableColumnType.HTML,
        id: 'typeView',
        label: this.translate.instant('monitoring.entity.clientType'),
        visibility: TableColumnVisibilityEnum.VISIBLE,
        sortable: false
      },
      {
        type: TableColumnType.HTML,
        id: 'terminalAcknowledgementView',
        label: this.translate.instant('monitoring.entity.terminalAcknowledgement'),
        visibility: TableColumnVisibilityEnum.VISIBLE,
        sortable: false
      },
      {
        type: TableColumnType.HTML,
        id: 'terminalGeneralStatusView',
        label: this.translate.instant('monitoring.entity.terminalGeneralStatus'),
        visibility: TableColumnVisibilityEnum.VISIBLE,
        sortable: false
      },
      {
        type: TableColumnType.HTML,
        id: 'terminalScenarioView',
        label: this.translate.instant('monitoring.entity.scenario'),
        visibility: TableColumnVisibilityEnum.VISIBLE,
        sortable: false
      },
      {
        type: TableColumnType.HTML,
        id: 'statusView',
        label: this.translate.instant('monitoring.entity.terminalStatus'),
        visibility: TableColumnVisibilityEnum.VISIBLE,
        sortable: false
      },
      {
        type: TableColumnType.HTML,
        id: 'terminalStartView',
        label: this.translate.instant('monitoring.entity.terminalStart'),
        visibility: TableColumnVisibilityEnum.VISIBLE,
        sortable: true
      }
    ];
  }

  getFilters(centersList: any): FilterBarItem[] {
    return [
      {
        id: 'centerName',
        type: FilterType.MULTIPLE_SELECTION_LIST,
        label: this.translate.instant('monitoring.entity.centerName'),
        shownByDefault: true,
        options: centersList
      },
      {
        id: 'terminalType',
        type: FilterType.SINGLE_SELECTION_LIST,
        label: this.translate.instant('monitoring.entity.terminalType'),
        shownByDefault: true,
        options: [
          { value: 'monitoring.clientType.terminal', label: this.translate.instant('monitoring.clientType.terminal') },
          { value: 'monitoring.clientType.viewer', label: this.translate.instant('monitoring.clientType.viewer') }
        ]
      },
      {
        id: 'terminalStatus',
        type: FilterType.SINGLE_SELECTION_LIST,
        label: this.translate.instant('monitoring.entity.terminalGeneralStatus'),
        shownByDefault: true,
        options: [
          { value: 'OK', label: this.translate.instant('monitoring.terminalStatus.ok') },
          // {value: 'WARNING', label:  this.translate.instant("monitoring.terminalStatus.warning")},
          // {value: 'WARNING_FRONTEND', label:  this.translate.instant("monitoring.terminalStatus.warning_frontend")},
          { value: 'ERROR', label: this.translate.instant('monitoring.terminalStatus.error') }
        ]
      },
      {
        id: 'terminalAck',
        type: FilterType.SINGLE_SELECTION_LIST,
        label: this.translate.instant('monitoring.entity.terminalAcknowledgement'),
        shownByDefault: true,
        options: [
          { value: 'ON', label: this.translate.instant('monitoring.terminalAck.on') },
          { value: 'OFF', label: this.translate.instant('monitoring.terminalAck.off') }
        ]
      },
      {
        id: 'terminalStartDate',
        type: FilterType.DATE_RANGE,
        label: this.translate.instant('monitoring.entity.terminalStart'),
        shownByDefault: true,
        showTimeRange: true
      },
    ];
  }
}
