import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { MonitoringComponent } from './components/monitoring.component';
import { MonitoringSearchBarComponent } from './components/monitoring-search-bar/monitoring-search-bar.component';
import { MonitoringSearchTableComponent } from './components/monitoring-search-table/monitoring-search-table.component';
import { AliveIconComponent } from './components/monitoring-search-table/alive-icon/alive-icon.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    MonitoringComponent,
    MonitoringSearchBarComponent,
    MonitoringSearchTableComponent,
    AliveIconComponent
  ],
  exports: [
    MonitoringComponent
  ],
  providers: [],
})
export class MonitoringModule {
}
