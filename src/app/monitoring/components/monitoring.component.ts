import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Terminal } from '../models/terminal.model';
import { TerminalMappingService } from '../services/terminal-mapping.service';
import { DateInputModel, FilterToValueMap, TableSortInfo } from 'bowl';
import { TitleBarParams } from '../../shared/model/title-bar.model';
import { TitleBarService } from '../../shared/services/title-bar.service';
import { TitleBarHandlerComponent } from '../../shared/components/title-bar-handler.component';
import { combineLatest, Observable, Subject, zip } from 'rxjs';
import { auditTime, takeUntil } from 'rxjs/operators';
import { TerminalCheckerProvider } from '../services/terminal-checker';
import * as moment from 'moment';

const QUERY_BASE = 'STATUS/';
const REFRESH_MS = 2000; // ms

@Component({
  selector: 'app-monitoring-search',
  templateUrl: './monitoring.component.html'

})
export class MonitoringComponent extends TitleBarHandlerComponent implements OnInit, OnDestroy {
  public static PAGE_TITLE = 'monitoring.home.search';
  titleBarParams: TitleBarParams = {
    pageTitle: MonitoringComponent.PAGE_TITLE,
    pageActions: null
  };
  firebaseList$ = new Observable<any>();
  search$ = new Subject<any>();
  resetPage$ = new Subject<any>();
  destroy$ = new Subject<any>();
  terminalsArray: Array<Terminal>;
  loading: boolean;
  dbService: any;
  filterList: any;
  tableSortInfo: TableSortInfo;
  private filter: any = {};

  constructor(
    private terminalMapping: TerminalMappingService,
    titleBarService: TitleBarService,
    db: AngularFireDatabase,
    public terminalCheckerService: TerminalCheckerProvider
  ) {
    super(titleBarService);
    this.dbService = db;
  }

  public static handleTitleBarAction(actionId: string): void {
    switch (actionId) {
      default:
        console.log('No actions available yet');
        break;
    }
  }

  ngOnInit(): void {
    // Notify and pass params to the title bar service, so it can notify subscribers
    this.notifyComponentInitialized(this.titleBarParams);

    // Subscribe to action requests that happens on the title bar
    this.titleBarService.actionRequested$.pipe(
      takeUntil(this.destroy$)
    ).subscribe(actionId => MonitoringComponent.handleTitleBarAction(actionId));

    // Subscription for monitoring search
    this.initSearchSubscriptions();
    this.search();
  }

  private initSearchSubscriptions(): void {
    this.firebaseList$ = this.dbService.list(QUERY_BASE).snapshotChanges().pipe(auditTime(REFRESH_MS));
    combineLatest(
      this.firebaseList$,
      this.search$
    ).pipe(takeUntil(this.destroy$)
    ).subscribe((response) =>
      this.applySearchParameters(QUERY_BASE, response[0])
    );
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.titleBarService.componentDestroyed();
    this.destroy$.next();
  }

  onSearchBasicFilter(searchValues: FilterToValueMap) {
    this.filter = searchValues;
    this.resetPage$.next();
    this.search();
  }

  onChangeOrder(orderValues: TableSortInfo) {
    this.tableSortInfo = orderValues;
    this.search();
  }

  search(): void {
    this.loading = true;
    this.search$.next();
  }

  // TODO MOVE TO EXTERNAL SERVICE
  private applySearchParameters(query: string, response: any): void {
    this.updateFilterList(query, response);

    // Check Filter Center Name
    this.terminalsArray = this.terminalMapping.mappingTerminals(response, this.filter.centerName);

    // Check Filter Terminal type
    if (this.filter.terminalType) {
      this.terminalsArray = this.filterTerminal(this.terminalsArray, 'ClientType', this.filter.terminalType);
    }

    // Check Filter Terminal Status
    if (this.filter.terminalStatus) {
      const valuesArray = this.filter.terminalStatus !== 'OK' ? ['WARNING', 'WARNING_FRONTEND', 'ERROR'] : ['OK'];
      this.terminalsArray = this.filterArrayTerminal(this.terminalsArray, 'GeneralStatus', valuesArray);
    }

    // Check Filter Terminal Activity
    if (this.filter.terminalAck) {
      this.terminalsArray = this.terminalsArray.filter(terminal => {
        const tillLastAckMillis = (Date.now() - (new Date(terminal.TerminalAcknowledgement)).getTime());
        return this.filter.terminalAck === 'OFF' ?
          tillLastAckMillis >= this.terminalCheckerService.config.terminalAckErrorMillis :
          tillLastAckMillis < this.terminalCheckerService.config.terminalAckErrorMillis;
      });
    }

    // Check Filter StartDate
    if (this.filter.terminalStartDate) {

      if (this.filter.terminalStartDate.from) {
        const fromDate = this.getMomentDateFromInputModel(this.filter.terminalStartDate.from).toDate();
        this.terminalsArray = this.terminalsArray.filter(terminal => {
          return (new Date(terminal.TerminalStart)).getTime() >= (new Date(fromDate)).getTime();
        });
      }

      if (this.filter.terminalStartDate.to) {
        const toDate = this.getMomentDateFromInputModel(this.filter.terminalStartDate.to).toDate();
        this.terminalsArray = this.terminalsArray.filter(terminal => {
          return (new Date(terminal.TerminalStart)).getTime() <= (new Date(toDate)).getTime();
        });
      }
    }

    // Check order
    if (this.tableSortInfo) {
      if (this.tableSortInfo.column === 'terminalStartView') {
        this.terminalsArray.sort((a, b) => {
          return this.tableSortInfo.order === 'DESC' ?
            new Date(b.TerminalStart).getTime() - new Date(a.TerminalStart).getTime() :
            new Date(a.TerminalStart).getTime() - new Date(b.TerminalStart).getTime();
        });
      } else if (this.tableSortInfo.column === 'CenterName') {
        this.terminalsArray.sort((a, b) => {
          return this.tableSortInfo.order === 'DESC' ?
            a.CenterName.localeCompare(b.CenterName) :
            b.CenterName.localeCompare(a.CenterName);
        });
      }
    } else {
      // Sort by default
      this.terminalsArray.sort((a, b) => {
        return new Date(b.TerminalStart).getTime() - new Date(a.TerminalStart).getTime();
      });
    }
    this.loading = false;
  }

  private filterTerminal(_terminalsArray, _key, _value): any {
    const filterByType = (terminal) => {
      return (terminal[_key] === _value);
    };
    return _terminalsArray.filter(filterByType, _value);
  }

  private filterArrayTerminal(_terminalsArray, _key, _values): any {
    const filterByType = (terminal) => {
      return (_values.indexOf(terminal[_key]) > -1);
    };
    return _terminalsArray.filter(filterByType, _values);
  }

  private updateFilterList(query: string, response): void {
    if (query === QUERY_BASE) {
      this.filterList = { centerName: this.terminalMapping.getCenterNameList(response) };
    }
  }

  private getMomentDateFromInputModel(dateInputModel: DateInputModel) {
    if (!dateInputModel) {
      return null;
    }
    return moment({
      year: dateInputModel.year,
      month: dateInputModel.month,
      day: dateInputModel.day,
      hour: dateInputModel.hour,
      minute: dateInputModel.minute,
      second: dateInputModel.second
    });
  }
}
