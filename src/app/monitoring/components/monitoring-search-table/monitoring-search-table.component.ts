import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MonitoringFactory } from '../../services/monitoring.factory';
import { TableLiterals, TablePagination, TableSortInfo } from 'bowl';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-monitoring-search-table',
  templateUrl: './monitoring-search-table.component.html',
  styleUrls: ['./monitoring-search-table.component.scss'],
})
export class MonitoringSearchTableComponent implements OnInit, OnChanges {
  @Input() rows: any[];
  @Input() loading: boolean;
  @Input() resetPage: Observable<any>;

  @Output() changeOrder = new EventEmitter<TableSortInfo>();
  @Output() changePageSize = new EventEmitter<number>();
  @Output() changeCurrentPage = new EventEmitter<number>();

  @ViewChild('terminalGeneralStatusView') terminalGeneralStatusView;
  @ViewChild('terminalAcknowledgeView') terminalAcknowledgeView;
  @ViewChild('terminalStartView') terminalStartView;

  @ViewChild('terminalScenarioView') terminalScenarioView;
  @ViewChild('statusView') statusView;
  @ViewChild('typeView') typeView;

  rowsDisplayed: any[];
  columns: any[];
  tableLiterals: TableLiterals;
  pagingData: TablePagination;
  defaultSort: TableSortInfo = { column: 'terminalStartView', order: 'DESC' };
  currentPage = 1;
  pageSize = 50;


  constructor(
    private monitoringFactory: MonitoringFactory
  ) {
  }

  ngOnInit(): void {
    this.generateTable();
    this.resetPage.subscribe(() => this.currentPage = 1);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes.rows && changes.rows.currentValue) {
      this.loadRows();
    }
  }

  onSortClick(event: TableSortInfo) {
    if (event) {
      this.currentPage = 1;
      this.changeOrder.emit(event);
    }
  }

  onPageSizeChanged(pageSize: number) {
    this.pageSize = pageSize;
    this.currentPage = 1;
    this.changePageSize.emit();
  }

  onPageChanged(pageSelected: number) {
    this.currentPage = pageSelected;
    this.changeCurrentPage.emit();
  }

  private generateTable(): void {
    this.columns = this.monitoringFactory.getColumns();
    this.tableLiterals = this.monitoringFactory.getTableLiterals();
    this.loadRows();
  }

  private loadRows(): void {
    if (this.rows) {
      this.pagingData = {
        currentPage: this.currentPage,
        pageSize: this.pageSize,
        totalPages: Math.ceil(this.rows.length / this.pageSize),
        totalItems: this.rows.length
      };
      const startPosition = (this.currentPage - 1) * this.pageSize;
      const endPosition = this.currentPage * this.pageSize;
      this.rowsDisplayed = this.rows.slice(startPosition, endPosition);

      this.rowsDisplayed.forEach((item) => {
        item['terminalAcknowledgementView'] = this.terminalAcknowledgeView;
        item['terminalStartView'] = this.terminalStartView;
        item['terminalGeneralStatusView'] = this.terminalGeneralStatusView;
        item['terminalScenarioView'] = this.terminalScenarioView;
        item['statusView'] = this.statusView;
        item['typeView'] = this.typeView;
      });
    }
  }
}
