import { Component, Input } from '@angular/core';
import { AliveStatus } from '../../../models/app.models';
import { TerminalCheckerProvider } from '../../../services/terminal-checker';


enum ConnectionClasses {
  on = 'icon-on',
  warning = 'icon-warning-off',
  off = 'icon-off'
}

@Component({
  selector: 'app-alive-icon',
  templateUrl: './alive-icon.component.html',
  styleUrls: ['./alive-icon.component.scss']
})
export class AliveIconComponent {
  connectionClass = ConnectionClasses.off;
  ack: any;
  @Input() set lastAck(ack: any) {
    this.ack = ack;
    this.connectionClass = this.isAliveClass(ack);
  }

  constructor(
    private checker: TerminalCheckerProvider
  ) {
  }

  private isAliveClass(lastAck: any): ConnectionClasses {
    const alive = this.checker.aliveStatus({ TerminalAcknowledgement: lastAck });
    switch (alive) {
      case AliveStatus.OK:
        return ConnectionClasses.on;
      case AliveStatus.WARNING:
        return ConnectionClasses.warning;
      case AliveStatus.KO:
        return ConnectionClasses.off;
    }
    return ConnectionClasses.on;
  }

}
