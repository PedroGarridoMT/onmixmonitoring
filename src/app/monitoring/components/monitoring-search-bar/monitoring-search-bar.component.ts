import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { FilterBarComponent, FilterBarItem, FilterBarLiterals, FilterChangeEvent, FilterToValueMap } from 'bowl';
import { MonitoringFactory } from '../../services/monitoring.factory';


@Component({
  selector: 'app-monitoring-search-bar',
  templateUrl: './monitoring-search-bar.component.html'
})
export class MonitoringSearchBarComponent implements OnInit, OnChanges {
  @Input() filterList: any;
  @Output() searchFilter = new EventEmitter<FilterToValueMap>();

  @ViewChild('filtersBar') filtersBar: FilterBarComponent;
  filters: FilterBarItem[];
  filterBarLiterals: FilterBarLiterals;
  valuesToSearch: FilterToValueMap = {};

  constructor(
    private monitoringFactory: MonitoringFactory
  ) {
  }

  private static isNewFilterList(changes: any): boolean {
    return !(MonitoringSearchBarComponent.equalsTwoObject(changes.filterList.currentValue, changes.filterList.previousValue));
  }

  private static equalsTwoObject(obj1, obj2): boolean {
    return JSON.stringify(obj1) === JSON.stringify(obj2);
  }

  ngOnInit(): void {
    this.loadFilters();
  }

  ngOnChanges(changes): void {
    if (changes && changes.filterList && MonitoringSearchBarComponent.isNewFilterList(changes)) {
      this.loadFilters();
    }
  }

  onSearchBasicFilter(searchValues: FilterToValueMap) {
    this.valuesToSearch = searchValues;
    this.searchFilter.emit(searchValues);
  }

  onClearBasicFilter() {
    this.valuesToSearch = null;
    this.searchFilter.emit({});
  }

  onFiltersChanged(event: FilterChangeEvent) {
    if (event.filterId === 'terminalStartDate') {
      if (this.setHourEndToFullDays(event.filterValue)) {
        this.filtersBar.updateFilter('terminalStartDate', false, [], this.getFilterDateAsFullDay(event.filterValue));
      }
    }
  }

  private loadFilters(): void {
    this.filterBarLiterals = this.monitoringFactory.getFiltersBarLiterals();
    this.filters = this.monitoringFactory.getFilters(this.getCenterList());
  }

  private getCenterList(): any {
    const centerList = [];
    if (!this.filterList || !this.filterList.centerName) {
      return centerList;
    }
    this.filterList.centerName.forEach((centerName) => {
      centerList.push({ value: centerName, label: centerName });
    });
    return centerList;
  }

  private setHourEndToFullDays(filterValue): boolean {
    return filterValue && this.sameDate(filterValue) && filterValue.to.hour === 0 && filterValue.to.minute === 0;
  }

  private getFilterDateAsFullDay(filterValue: any): any {
    filterValue.to.hour = 23;
    filterValue.to.minute = 59;
    filterValue.to.second = 59;
    return filterValue;
  }

  private sameDate(filterValue): boolean {
    const dateFrom = filterValue.from;
    const dateTo = filterValue.to;
    if (dateFrom && dateTo) {
      return dateFrom.year === dateTo.year &&
        dateFrom.month === dateTo.month && dateFrom.day === dateTo.day;
    }
    return false;
  }
}
