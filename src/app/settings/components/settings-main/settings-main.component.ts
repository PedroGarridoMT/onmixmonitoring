import { Component, OnInit, ViewChild } from '@angular/core';
import { HomeTemplateDashboardItem, Language } from 'bowl';
import { TranslateService } from '@ngx-translate/core';
import { ConfigService } from '../../../core/config.service';

@Component({
  templateUrl: './settings-main.component.html',
  styleUrls: ['./settings-main.component.scss']

})
export class SettingsMainComponent implements OnInit {
  appLanguages: Language[] = null;
  dashboardItems: HomeTemplateDashboardItem[];
  @ViewChild('changeLanguageTemplate', { static: true }) changeLanguageTemplate;

  constructor(private translate: TranslateService, private config: ConfigService) {
    this.appLanguages = config.getKey('languages') as Language[];
    this.appLanguages.forEach(lang => {
      lang.selected = this.translate.currentLang ? (this.translate.currentLang.substring(0, 2) === lang.value) : false;
    });
  }

  ngOnInit() {
    this.dashboardItems = [
      { template: this.changeLanguageTemplate, templateContext: null }
    ];
  }

  onLanguageChanged(selectedLang) {
    this.config.setLanguageAndLocale(selectedLang);
  }
}
