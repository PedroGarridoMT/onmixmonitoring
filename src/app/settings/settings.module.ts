import { NgModule } from '@angular/core';
import { SettingsMainComponent } from './components/settings-main/settings-main.component';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from '../app-routing.module';

@NgModule({
  imports: [
    AppRoutingModule,
    SharedModule
  ],
  declarations: [
    SettingsMainComponent
  ],
  exports: [
    SettingsMainComponent
  ],
  providers: [],
})
export class SettingsModule {
}
